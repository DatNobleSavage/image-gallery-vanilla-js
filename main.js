const current = document.querySelector('#current');
const imgs = document.querySelectorAll('.imgs img');
const opacity = 0.6;

imgs.forEach(img => img.addEventListener('click', imgClick));

// Set initial image opacity
imgs[0].style.opacity = opacity;

function imgClick(e) {
  // Reset opacity
  imgs.forEach(img => (img.style.opacity = 1));

  // Change current image to src of selected image
  current.src = e.target.src;

  // Add fade-in class
  current.classList.add('fade-in');

  // Reset fade-in class after 500ms
  setTimeout(() => current.classList.remove('fade-in'), 500);

  // Change opacity to var value
  e.target.style.opacity = opacity;
}
